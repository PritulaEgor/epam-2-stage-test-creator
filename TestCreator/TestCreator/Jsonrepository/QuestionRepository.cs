﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestCreator.Jsonrepository
{
    class QuestionRepository
    {
        private string filePath;

        private string answerFilepath;
        private List<Question> ReadQuestion()
        {
            List<Question> questions = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                questions = JsonSerializer.Deserialize<List<Question>>(str);
            }
            catch
            {
                questions = new List<Question> { };
            }

            return questions;
        }
        public void AddNewQuestion(Question question)
        {

            List<Question> questions = ReadQuestion();
            try
            {
                questions.Add(question);
            }
            catch
            {
                questions = new List<Question> { question };
            }

            string json = JsonSerializer.Serialize(questions);

            File.WriteAllText(filePath, json); ;
        }
    }
}
