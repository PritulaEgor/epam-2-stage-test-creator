﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestCreator.Jsonrepository
{
    class SubTopicRepository
    {
        private string filePath;

        private string subTopicsFilepath;

        public SubTopicRepository(SubTopic subTopic)
        {
            if (!Directory.Exists(@$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}");
            }
            this.filePath = @$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}/{subTopic.Name}.json";

            this.subTopicsFilepath = @$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/subtopics.json";
        }
        private List<SubTopic> ReadSubTopic()
        {
            List<SubTopic> subTopics = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                subTopics = JsonSerializer.Deserialize<List<SubTopic>>(str);
            }
            catch
            {
                subTopics = new List<SubTopic> { };
            }

            return subTopics;
        }
        private List<SubTopic> ReadSubTopics()
        {
            List<SubTopic> subTopics = null;

            if (File.Exists(subTopicsFilepath))
            {
                StreamReader streamReader = new StreamReader(subTopicsFilepath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                subTopics = JsonSerializer.Deserialize<List<SubTopic>>(str);
            }
            else
            {
                subTopics = new List<SubTopic> { };
            }

            return subTopics;
        }
        public void AddNewSubTopic(SubTopic subTopic)
        {

            List<SubTopic> subTopics = ReadSubTopic();
            try
            {
                subTopics.Add(subTopic);
            }
            catch
            {
                subTopics = new List<SubTopic> { subTopic };
            }

            string json = JsonSerializer.Serialize(subTopics);

            File.WriteAllText(filePath, json);

            subTopics = ReadSubTopics();
            try
            {
                subTopics.Add(subTopic);
            }
            catch
            {
                subTopics = new List<SubTopic> { subTopic };
            }
            json = JsonSerializer.Serialize(subTopics);

            File.WriteAllText(subTopicsFilepath, json);
        }
    }
}
