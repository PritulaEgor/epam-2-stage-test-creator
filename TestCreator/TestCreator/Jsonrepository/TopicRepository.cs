﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestCreator.Jsonrepository
{
    class TopicRepository
    {
        private string filePath;

        private string topicsFilepath;
        public TopicRepository(Topic topic)
        {
            if (!Directory.Exists(@$"../Tests/{topic.TestName}/{topic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{topic.TestName}/{topic.Name}");
            }
            this.filePath = @$"../Tests/{topic.TestName}/{topic.Name}/{topic.Name}.json";

            this.topicsFilepath = @$"../Tests/{topic.TestName}/topics.json";
        }
        private List<Topic> ReadTopic()
        {
            List<Topic> topics = null;

            if(File.Exists(filePath))
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                topics = JsonSerializer.Deserialize<List<Topic>>(str);
            }
            else
            {
                topics = new List<Topic> { };
            }

            return topics;
        }
        private List<Topic> ReadTopics()
        {
            List<Topic> topics = null;

            if(File.Exists(topicsFilepath))
            {
                StreamReader streamReader = new StreamReader(topicsFilepath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                topics = JsonSerializer.Deserialize<List<Topic>>(str);
            }
            else
            {
                topics = new List<Topic> { };
            }

            return topics;
        }
        public void AddNewTopic(Topic topic)
        {

            List<Topic> topics = ReadTopic();
            try
            {
                topics.Add(topic);
            }
            catch
            {
                topics = new List<Topic> { topic };
            }

            string json = JsonSerializer.Serialize(topics);

            File.WriteAllText(filePath, json);

            topics = ReadTopics();
            try
            {
                topics.Add(topic);
            }
            catch
            {
                topics = new List<Topic> { topic };
            }
            json = JsonSerializer.Serialize(topics);

            File.WriteAllText(topicsFilepath, json);
        }
    }
}
