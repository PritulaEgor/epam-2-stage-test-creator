﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestCreator.Jsonrepository
{
    class TestRepository
    {
        private string filePath;

        public TestRepository(Test test)
        {
            if (!Directory.Exists(@$"../Tests/{test.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{test.Name}");
            }
            this.filePath = @$"../Tests/{test.Name}/{test.Name}.json";
        }
        private List<Test> ReadTests()
        {
            List<Test> tests = null;

            try
            {
                StreamReader streamReader = new StreamReader(@"../Tests/Tests.json");

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                tests = JsonSerializer.Deserialize<List<Test>>(str);
            }
            catch
            {
                tests = new List<Test> { };
            }

            return tests;
        }
        private List<Test> ReadTest()
        {
            List<Test> tests = null;

            if(File.Exists(filePath))
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                tests = JsonSerializer.Deserialize<List<Test>>(str);
            }
            else
            {
                tests = new List<Test> { };
            }

            return tests;
        }

        public void AddNewTest(Test test)
        {

            List<Test> tests = ReadTest();
            try
            {
                tests.Add(test);
            }
            catch
            {
                tests = new List<Test> { test };
            }

            string json = JsonSerializer.Serialize(tests);

            //if (!File.Exists(filePath))
            //{
            //    File.Create(filePath);
            //}

            File.WriteAllText(filePath, json);

            tests = ReadTests();
            try
            {
                tests.Add(test);
            }
            catch
            {
                tests = new List<Test> { test };
            }
            json = JsonSerializer.Serialize(tests);

            File.WriteAllText(@"../Tests/Tests.json", json);
        }
    }
}
