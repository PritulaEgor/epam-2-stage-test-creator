﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestCreator.Jsonrepository;

namespace TestCreator
{
    static class TestCreationMenu
    {
        private static Test test;
        private static List<Topic> topics = new List<Topic>();
        private static List<SubTopic> subTopics = new List<SubTopic>();
        private static List<Question> questions;
        private static bool continueWork = true;
        public static void Start()
        {
            AddTest();

            while (continueWork)
            {
                RenderTable();

                CheckButtonMenue();
            }
        }
      static  void RenderTable()
        {
            Console.Clear();

            Console.WriteLine("Press specified buttons to perform an operation:\n" +
                "\t1 - Add topic\n" +
                "\t2 - Add subtopic(to existent topic)\n" +
                "\t3 - Add question \n" + 
                "\tF1 - Exit");

        }
     
        private static void AddSubtopic()
        {
            if (topics.Count != 0)
            {

                SubTopic newSubTopic = new SubTopic();
                newSubTopic.TestName = test.Name;
                int sBT;

                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("Choose which topic to add a sub-topic");



                    for (int i = 0; i < topics.Count; i++)
                    {
                        Console.WriteLine($"Topic {i + 1}:{topics[i].Name}");
                    }

                    try
                    {
                        ConsoleKeyInfo button = Console.ReadKey();
                        string sSBT = Convert.ToString(button.KeyChar);
                        sBT = Convert.ToInt32(sSBT);
                        newSubTopic.TopicName = topics[sBT-1].Name;
                        break;
                    }
                    catch (FormatException)
                    {
                        
                    }
                    catch (ArgumentOutOfRangeException)
                    {

                    }

                }

                Console.Clear();
                Console.WriteLine("Write the name of the subtopic");

                string name = Console.ReadLine();

                newSubTopic.Name = name.Replace(' ', '_');

                SubTopicRepository subTopicR = new SubTopicRepository(newSubTopic);

                subTopicR.AddNewSubTopic(newSubTopic);

                subTopics.Add(newSubTopic);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("You haven't created any topics\n" +
                    "Press any button to continue...");
                Console.ReadKey();
            }

            }
        private static void AddTopic()
        {
            Console.Clear();

            Topic newTopic = new Topic();

            Console.WriteLine("Write the name of the topic");

            string name = Console.ReadLine();

            newTopic.Name = name.Replace(' ', '_');

            newTopic.TestName = test.Name;

            TopicRepository topicR = new TopicRepository(newTopic);

            topicR.AddNewTopic(newTopic);

            topics.Add(newTopic);
            
        }
        private static void AddTest()
        {
           
            Test newTest = new Test();

            Console.WriteLine("Write the name of your test");
            string name = Console.ReadLine();
           
            newTest.Name = name.Replace(' ', '_');

            newTest.AnswerOption = YesOrNot("Are your test will be with answer options?");

            newTest.ShowCorrectAnswer = YesOrNot("Show correct answer when user made a choise?");

            newTest.AreTimerNeeded = YesOrNot("Set a timer?");

            TestRepository testR = new TestRepository(newTest);

            testR.AddNewTest(newTest);

            test = newTest;
        } 
        private static void CheckButtonMenue()
        {
            ConsoleKeyInfo button = Console.ReadKey();
            if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
            {
                AddTopic();
            }
            else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '2')
            {
                AddSubtopic();
            }
            else if (button.Key == ConsoleKey.NumPad3 || button.KeyChar == '3')
            {
              
            }
            else if (button.Key == ConsoleKey.F1)
            {
                continueWork = false;
            }
        }

        private static string YesOrNot(string message)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(message);
                Console.WriteLine("1.Yes\n" +
                    "2.No");
                ConsoleKeyInfo button = Console.ReadKey();

                if (button.Key == ConsoleKey.D1)
                {
                    return "true";
                }
                else if (button.Key == ConsoleKey.D2)
                {
                    return "false";
                }
            }
        }
    }
}
