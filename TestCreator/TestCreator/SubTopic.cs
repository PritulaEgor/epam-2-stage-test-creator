﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class SubTopic
    {



        private string topicName;

        public string TopicName
        {
            get
            {
                return topicName;
            }
            set
            {
                topicName = value;
            }
        }

        private string testName;
        public string TestName
        {
            get
            {
                return testName;
            }
            set
            {
                testName = value;
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }


        private int countOfSubtopics = 0;

        public int CountOfSubTopics
        {
            get
            {
                return countOfSubtopics;
            }
            set
            {
                if (value >= 0)
                {
                    countOfSubtopics = value;
                }
            }
        }

        private int countOfQuestions = 0;

        public int CountOfQuestions
        {
            get
            {
                return countOfQuestions;
            }
            set
            {
                if (value >= 0)
                {
                    countOfQuestions = value;
                }
            }
        }

        //private List<SubTopic> subTopicList;

        //private List<Question> questionList;
    }
}
