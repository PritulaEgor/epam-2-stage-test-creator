﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class Question
    {

        private int number = 0;
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                if (value > 0)
                {
                    number = value;
                }
            }
        }

        private string text;

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        private int countOfAnswers;

        public int CountOfAnswers
        {
            get
            {
                return countOfAnswers;
            }
            set
            {
                if (value >= 0)
                {
                    countOfAnswers = value;
                }
            }
        }

        private int countOfRightAnswers;

        public int CountOfRightAnswers
        {
            get
            {
                return countOfRightAnswers;
            }
            set
            {
                if (value >= 0 || value <= countOfAnswers)
                {
                    countOfRightAnswers = value;
                }
            }
        }

        private string testName;

        public string TestName
        {
            get
            {
                return testName;
            }
            set
            {
                testName = value;
            }
        }

        private string topicName;
        public string TopicName
        {
            get
            {
                return topicName;
            }
            set
            {
                topicName = value;
            }
        }

        private string subTopicName=null;
        public string SubTopicName
        {
            get
            {
                return subTopicName;
            }
            set
            {
                subTopicName = value;
            }
        }

        private List<Answer> answersList;
    }
}
