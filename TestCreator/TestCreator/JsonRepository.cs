﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace TestCreator
{

    //Идет под удаление, уже не нужен, но пока не все скопировал с него))) поэтому не удалил
    class JsonRepository
    {
        private string filePath = @"../Tests/Tests.json";

        public JsonRepository(Test test)
        {
            if (!Directory.Exists(@$"../Tests/{test.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{test.Name}");
            }
            this.filePath = @$"../Tests/{test.Name}/{test.Name}.json";
        }

        public JsonRepository(Topic topic)
        {
            if (!Directory.Exists(@$"../Tests/{topic.TestName}/{topic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{topic.TestName}/{topic.Name}");
            }
            this.filePath = @$"../Tests/{topic.TestName}/{topic.Name}/{topic.Name}.json";

        }

        public JsonRepository(SubTopic subTopic)
        {
            if (!Directory.Exists(@$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}"))
            {
                Directory.CreateDirectory(@$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}");
            }
            this.filePath = @$"../Tests/{subTopic.TestName}/{subTopic.TopicName}/{subTopic.Name}/{subTopic.Name}.json";
        }

        public JsonRepository(Question question)
        {
            if (question.SubTopicName != null)
            {
                if (!Directory.Exists(@$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question"))
                {
                    Directory.CreateDirectory(@$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question");
                }

                this.filePath = @$"../Tests/{question.TestName}/{question.TopicName}/{question.SubTopicName}/{question.Number} question/{question.Number}.json";
            }
            else
            {
                if (!Directory.Exists(@$"../Tests/{question.TestName}/{question.TopicName}/{question.Number} question"))
                {
                    Directory.CreateDirectory(@$"../Tests/{question.TestName}/{question.TopicName}/{question.Number} question");
                }

                this.filePath = @$"../Tests/{question.TestName}/{question.TopicName}/{question.Number} question/{question.Number}.json";

            }
        }
       
        //public void CreateFile()
        //{
        //    if (!File.Exists(filePath))
        //    {
        //        File.Create(filePath);
        //    }

        //}
        private List<Answer> ReadAnswers()
        {
            List<Answer> answers = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                answers = JsonSerializer.Deserialize<List<Answer>>(str);
            }
            catch
            {
                answers = new List<Answer> { };
            }

            return answers;
        }
        private List<Test> ReadTests()
        {
            List<Test> tests = null;

            try
            {
                StreamReader streamReader = new StreamReader(@"../Tests/Tests.json");

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                tests = JsonSerializer.Deserialize<List<Test>>(str);
            }
            catch
            {
                tests = new List<Test> { };
            }

            return tests;
        }
        private List<Test> ReadTest()
        {
            List<Test> tests = null;

            if(File.Exists(filePath))
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                tests = JsonSerializer.Deserialize<List<Test>>(str);
            }
            else
            {
                tests = new List<Test> { };
            }

            return tests;
        }

        private List<Topic> ReadTopic()
        {
            List<Topic> topics = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                topics = JsonSerializer.Deserialize<List<Topic>>(str);
            }
            catch 
            {
                topics = new List<Topic> { };
            }

            return topics;
        }
        private List<SubTopic> ReadSubTopic()
        {
            List<SubTopic> subTopics = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                subTopics = JsonSerializer.Deserialize<List<SubTopic>>(str);
            }
            catch 
            {
                subTopics = new List<SubTopic> { };
            }

            return subTopics;

        }
        private List<Question> ReadQuestion()
        {
            List<Question> questions = null;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);

                var str = streamReader.ReadToEnd();

                streamReader.Close();

                questions = JsonSerializer.Deserialize<List<Question>>(str);
            }
            catch 
            {
                questions = new List<Question> { };
            }

            return questions;
        }
        public void AddNewTest(Test test)
        {

            List<Test> tests = ReadTest();
            try
            {
                tests.Add(test);
            }
            catch
            {
                tests = new List<Test> { test };
            }

            string json = JsonSerializer.Serialize(tests);

            //if (!File.Exists(filePath))
            //{
            //    File.Create(filePath);
            //}

            File.WriteAllText(filePath, json);

            tests = ReadTests();
            try
            {
                tests.Add(test);
            }
            catch
            {
                tests = new List<Test> { test };
            }
            json = JsonSerializer.Serialize(tests);

            File.WriteAllText(@"../Tests/Tests.json", json);
        }

        public void AddNewTopic(Topic topic)
        {

            List<Topic> topics = ReadTopic();
            try
            {
                topics.Add(topic);
            }
            catch
            {
                topics = new List<Topic> { topic };
            }

            string json = JsonSerializer.Serialize(topics);

            File.WriteAllText(filePath, json); ;
        }
        public void AddNewSubTopic(SubTopic subTopic)
        {

            List<SubTopic> subTopics = ReadSubTopic();
            try
            {
                subTopics.Add(subTopic);
            }
            catch
            {
                subTopics = new List<SubTopic> { subTopic };
            }

            string json = JsonSerializer.Serialize(subTopics);

            File.WriteAllText(filePath, json); ;
        }

        public void AddNewQuestion(Question question)
        {

            List<Question> questions = ReadQuestion();
            try
            {
                questions.Add(question);
            }
            catch
            {
                questions = new List<Question> { question };
            }

            string json = JsonSerializer.Serialize(questions);

            File.WriteAllText(filePath, json); ;
        }


        //Just for testing//
        public Test returntest()
        {
            List<Test> tests = ReadTest();
            //Console.WriteLine(filePath);
            //Console.ReadKey();
            return tests[0];
        }
    }
}
