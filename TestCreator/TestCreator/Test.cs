﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCreator
{
    class Test
    {
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        private string answerOption;

        public string AnswerOption
        {
            get
            {
                return answerOption;
            }
            set
            {
                if (value == "true" || value == "false")
                {
                    answerOption = value;
                }
            }
        }

        private string showCorrectAnswer;
        public string ShowCorrectAnswer
        {
            get
            {
                return showCorrectAnswer;
            }
            set
            {
                if (value == "true" || value == "false")
                {
                    showCorrectAnswer = value;
                }
            }
        }
        //public bool ShowCorrectAnswer
        //{
        //    get
        //    {
        //        return Convert.ToBoolean(showCorrectAnswer);
        //    }
        //    set
        //    {
        //        showCorrectAnswer = Convert.ToString(value);
        //    }
        //}


        private string areTimerNeeded;

        public string AreTimerNeeded
        {
            get
            {
                return areTimerNeeded;
            }
            set
            {
                if (value == "true" || value == "false")
                {
                    areTimerNeeded = value;
                }
            }
        }

        //private DateTime timer;

        private int countOfTopics = 0;

        public int CountOfTopics
        {
            get
            {
                return countOfTopics;
            }
            set
            {
                if (value >= 0)
                {
                    countOfTopics = value;
                }
            }

        }
    }
}
